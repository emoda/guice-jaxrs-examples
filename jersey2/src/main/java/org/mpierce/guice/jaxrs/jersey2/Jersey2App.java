package org.mpierce.guice.jaxrs.jersey2;

import com.google.inject.Inject;
import org.glassfish.jersey.CommonProperties;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.mpierce.guice.jaxrs.common.SampleResource;

final class Jersey2App extends ResourceConfig {

    @Inject
    Jersey2App(ObjectMapperContextResolver objectMapperContextResolver) {
        register(objectMapperContextResolver);

        register(SampleResource.class);
        register(JacksonFeature.class);

        property(CommonProperties.METAINF_SERVICES_LOOKUP_DISABLE, true);
        property(CommonProperties.FEATURE_AUTO_DISCOVERY_DISABLE, true);
        property(CommonProperties.JSON_PROCESSING_FEATURE_DISABLE, true);
        property(CommonProperties.MOXY_JSON_FEATURE_DISABLE, true);
    }
}
