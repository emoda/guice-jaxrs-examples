package org.mpierce.guice.jaxrs.jersey1;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.servlet.ServletModule;
import com.palominolabs.http.server.HttpServerConnectorConfig;
import com.palominolabs.http.server.HttpServerWrapperConfig;
import com.palominolabs.http.server.HttpServerWrapperFactory;
import com.palominolabs.http.server.HttpServerWrapperModule;
import com.sun.jersey.guice.JerseyServletModule;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;
import org.mpierce.guice.jaxrs.common.JacksonModule;
import org.mpierce.guice.jaxrs.common.SampleResourceModule;
import org.slf4j.bridge.SLF4JBridgeHandler;

import java.util.logging.LogManager;

class Jersey1Main {

    private static final String HOST = "localhost";
    private static final int PORT = 8080;

    public static void main(String[] args) throws Exception {

        LogManager.getLogManager().reset();
        SLF4JBridgeHandler.install();

        // start up an http server
        HttpServerWrapperConfig config = new HttpServerWrapperConfig()
            .withHttpServerConnectorConfig(HttpServerConnectorConfig.forHttp(HOST, PORT));

        getInjector().getInstance(HttpServerWrapperFactory.class)
            .getHttpServerWrapper(config)
            .start();
    }

    private static Injector getInjector() {
        return Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                binder().requireExplicitBindings();

                install(new HttpServerWrapperModule());
                install(new SampleResourceModule());
                install(new JacksonModule());

                // standard jersey-guice module
                install(new JerseyServletModule());

                // register metric filter as well as new relic transaction name filter
                install(new ServletModule() {
                    @Override
                    protected void configureServlets() {
                        // jersey-guice integration
                        bind(GuiceContainer.class);
                        serve("/*").with(GuiceContainer.class);
                    }
                });
            }

            @Provides
            @Singleton
            JacksonJsonProvider getJacksonJsonProvider(ObjectMapper objectMapper) {
                return new JacksonJsonProvider(objectMapper);
            }
        });
    }
}
